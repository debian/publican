# AUTHOR <EMAIL@ADDRESS>, YEAR.
# anipeter <anipeter@fedoraproject.org>, 2012. #zanata
msgid ""
msgstr ""
"Project-Id-Version: 0\n"
"POT-Creation-Date: 2015-02-25 14:40+1000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2012-08-30 03:00-0400\n"
"Last-Translator: anipeter <anipeter@fedoraproject.org>\n"
"Language-Team: None\n"
"Language: ml\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"X-Generator: Zanata 3.6.0\n"

# translation auto-copied from project Publican Common, version 3, document Revision_History
msgid "Revision History"
msgstr "റിവിഷന്‍ ഹിസ്റ്ററി"

# translation auto-copied from project Publican Common, version 3, document Revision_History, author anipeter
msgid "Jeff"
msgstr "ജെഫ്"

# translation auto-copied from project Publican Common, version 3, document Revision_History, author anipeter
msgid "Fearn"
msgstr "ഫേണ്‍"

# translation auto-copied from project Publican Common, version 3, document Revision_History, author anipeter
msgid "Publican 3.0"
msgstr "പബ്ലിക്കന്‍ 3.0"

