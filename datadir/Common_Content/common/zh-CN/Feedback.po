# AUTHOR <EMAIL@ADDRESS>, YEAR.
# translation of Feedback.po to Simplified Chinese
# Xi HUANG <xhuang@redhat.com>, 2007.
# Leah Liu <lliu@redhat.com>, 2008.
# translation of Feedback.po to
# leahliu <lliu@redhat.com>, 2015. #zanata
msgid ""
msgstr ""
"Project-Id-Version: 0\n"
"POT-Creation-Date: 2015-02-25 14:40+1000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2015-03-02 05:12-0500\n"
"Last-Translator: leahliu <lliu@redhat.com>\n"
"Language-Team: Simplified Chinese <zh@li.org>\n"
"Language: zh-Hans-CN\n"
"Plural-Forms: nplurals=1; plural=0\n"
"X-Generator: Zanata 3.6.0\n"

# translation auto-copied from project Publican Common, version 3, document Feedback
msgid "We Need Feedback!"
msgstr "我们需要您的反馈意见！"

# translation auto-copied from project Publican Common, version 3, document Feedback
msgid "<primary>feedback</primary> <secondary>contact information for this manual</secondary>"
msgstr "<primary>反馈</primary> <secondary>本手册联系人信息</secondary>"

# translation auto-copied from project Publican Common, version 3, document Feedback
msgid "You should over ride this by creating your own local Feedback.xml file."
msgstr "您可以创建自己的本地 Feedback.xml 文件覆盖这个文件。"

