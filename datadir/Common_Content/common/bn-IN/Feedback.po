# AUTHOR <EMAIL@ADDRESS>, YEAR.
# translation of Feedback.po to Bengali INDIA
# Runa Bhattacharjee <runab@redhat.com>, 2007, 2008.
msgid ""
msgstr ""
"Project-Id-Version: 0\n"
"POT-Creation-Date: 2015-02-25 14:40+1000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2008-10-07 01:48-0400\n"
"Last-Translator: Runa Bhattacharjee <runab@redhat.com>\n"
"Language-Team: Bengali INDIA <fedora-trans-bn_IN@redhat.com>\n"
"Language: bn-IN\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Zanata 3.6.0\n"

# translation auto-copied from project Publican Common, version 3, document Feedback
msgid "We Need Feedback!"
msgstr "আপনার মতামত আমাদের নিশ্চয় জানাবেন!"

# translation auto-copied from project Publican Common, version 3, document Feedback
msgid "<primary>feedback</primary> <secondary>contact information for this manual</secondary>"
msgstr "<primary>মতামত</primary> <secondary>এই সহায়ক-পুস্তিকা সম্বন্ধে যোগাযোগের তথ্য</secondary>"

# translation auto-copied from project Publican Common, version 3, document Feedback
msgid "You should over ride this by creating your own local Feedback.xml file."
msgstr "এই ফাইলটি অগ্রাহ্য করার জন্য নিজস্ব Feedback.xml ফাইল নির্মাণ করুন।"

