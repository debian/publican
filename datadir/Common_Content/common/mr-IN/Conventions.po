# translation of Conventions.po to Marathi
# AUTHOR <EMAIL@ADDRESS>, YEAR.
# sandeep shedmake <sandeep.shedmake@gmail.com>, 2007.
# Sandeep Shedmake <sandeep.shedmake@gmail.com>, 2008.
# Sandeep Shedmake <sshedmak@redhat.com>, 2009, 2013.
# sandeeps <sandeeps@fedoraproject.org>, 2012. #zanata
msgid ""
msgstr ""
"Project-Id-Version: 0\n"
"POT-Creation-Date: 2015-02-25 14:40+1000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2013-09-02 01:28-0400\n"
"Last-Translator: Sandeep Shedmake <sshedmak@redhat.com>\n"
"Language-Team: Marathi\n"
"Language: mr\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Zanata 3.6.0\n"
"X-Project-Style: kde\n"

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "Document Conventions"
msgstr "दस्तऐवज नियमावली"

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "This manual uses several conventions to highlight certain words and phrases and draw attention to specific pieces of information."
msgstr "ठराविक शब्द व वाक्यरचना ठळक करण्याकरीता व ठराविक माहितीकडे लक्ष वेधण्याकरीता ही पुस्तिका विविध नियमावली वापरते."

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "Typographic Conventions"
msgstr "टायपोग्राफीक नियमावली"

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "Four typographic conventions are used to call attention to specific words and phrases. These conventions, and the circumstances they apply to, are as follows."
msgstr "ठराविक शब्द व वाक्यरचना करीता लक्ष वेधण्याकरीता चार टायपोग्राफिक नियमावली वापरले जाते. या नियमावली, व त्याच्याशी संबंधीत लागू करण्याजोगी वातावरण, खालिल नुरूप आहेत."

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "<literal>Mono-spaced Bold</literal>"
msgstr "<literal>मोनो-स्पेस्ड् बोल्ड</literal>"

# translation auto-copied from project Publican Common, version 3, document Conventions, author sandeeps
msgid "Used to highlight system input, including shell commands, file names and paths. Also used to highlight keys and key combinations. For example:"
msgstr "प्रणाली इन्पुट, सर्व शेल आदेश, फाइल नावे व मार्ग ठळक करण्यासाठी वापरले जाते. कि व कि जोडणी ठळक करण्यासाठी देखिल वापरले जाते. उदाहरणार्थ:"

# translation auto-copied from project Publican Common, version 3, document
# Conventions
msgid "To see the contents of the file <filename>my_next_bestselling_novel</filename> in your current working directory, enter the <command>cat my_next_bestselling_novel</command> command at the shell prompt and press <keycap>Enter</keycap> to execute the command."
msgstr "सध्याच्या कार्रत डिरेक्ट्रीमध्ये <filename>my_next_bestselling_novel</filename> फाइलची अंतर्भुत माहिती पाहण्याकरीता, शेल प्रॉम्पट वर <command>cat my_next_bestselling_novel</command> आदेश द्या व आदेश चालविण्याकरीता <keycap>Enter</keycap> दाबा."

# translation auto-copied from project Publican Common, version 3, document Conventions, author sandeeps
msgid "The above includes a file name, a shell command and a key, all presented in mono-spaced bold and all distinguishable thanks to context."
msgstr "वरीलमध्ये पाइलचे नाव, शेल आदेश व कि समाविष्टीत आहे, सर्व मोनो-स्पेस्ड् बोल्डमध्ये प्रस्तुत केले जातात व सगळ्यांचा संदर्भ वेगळा असतो."

# translation auto-copied from project Publican Common, version 3, document Conventions, author sandeeps
msgid "Key combinations can be distinguished from an individual key by the plus sign that connects each part of a key combination. For example:"
msgstr "प्लस् चिन्हाच्या मदतीने, जे कि जोडणीला प्रत्येक भागासह जुळते; व्यक्तिगत किपासून कि जोडणीला वेगळे करणे शक्य आहे. उदाहरणार्थ:"

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "Press <keycap>Enter</keycap> to execute the command."
msgstr "आदेश चालविण्याकरीता <keycap>Enter</keycap> दाबा."

# translation auto-copied from project Publican Common, version 3, document Conventions, author sandeeps
msgid "Press <keycombo><keycap>Ctrl</keycap><keycap>Alt</keycap><keycap>F2</keycap></keycombo> to switch to a virtual terminal."
msgstr "वर्च्युअल टर्मिनलचा वापर करण्यासाठी <keycombo><keycap>Ctrl</keycap><keycap>Alt</keycap><keycap>F2</keycap></keycombo> दाबा."

# translation auto-copied from project Publican Common, version 3, document Conventions, author sandeeps
msgid "The first example highlights a particular key to press. The second example highlights a key combination: a set of three keys pressed simultaneously."
msgstr "पहिले उदाहरण दाबण्याजोगी ठराविक किला ठळक करतो. दुसरे उदाहरण कि जोडणी ठळक करतो: तीन किज्ला एकाचवेळी दाबल्यास."

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "If source code is discussed, class names, methods, functions, variable names and returned values mentioned within a paragraph will be presented as above, in <literal>mono-spaced bold</literal>. For example:"
msgstr "सोअर्स् कोडचा उल्लेख केल्यावर परिच्छेदातील, क्लास्चे नाव, मेथड्स्, फंक्शन्स्, वेरियेबल नावे व रिटअर्न मूल्य <literal>मोनो-स्पेस्ड् बोल्ड</literal> मध्ये प्रस्तूत केले जातील. उदाहरणार्थ:"

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "File-related classes include <classname>filesystem</classname> for file systems, <classname>file</classname> for files, and <classname>dir</classname> for directories. Each class has its own associated set of permissions."
msgstr "फाइल-संबंधित वर्ग अंतर्गत फाइल प्रणाली करीता <classname>filesystem</classname>, फाइल करीता <classname>file</classname>, व संचयीका करीता <classname>dir</classname> समाविष्टीत आहे. प्रत्येक वर्गकाकडे संबंधित परवानगी संच समाविष्ठीत आहे."

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "<application>Proportional Bold</application>"
msgstr "<application>प्रप्रोर्श्नल बोल्ड</application>"

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "This denotes words or phrases encountered on a system, including application names; dialog-box text; labeled buttons; check-box and radio-button labels; menu titles and submenu titles. For example:"
msgstr "हे प्रणालीवरील शब्द किंवा वाक्यरचना दाखवते, ज्यामध्ये ऍनीमेशनचे नाव; संवादपेटीतील मजकूर; लेबल बटने; चेक-बॉक्स् व रेडिओ बटन लेबल्स्; मेन्यू शिर्षक व उप-मेन्यू शिर्षकाचे समावेश आहे. उदाहरणार्थ:"

msgid "Choose <menuchoice><guimenu>System</guimenu><guisubmenu>Preferences</guisubmenu><guimenuitem>Mouse</guimenuitem></menuchoice> from the main menu bar to launch <application>Mouse Preferences</application>. In the <guilabel>Buttons</guilabel> tab, select the <guilabel>Left-handed mouse</guilabel> check box and click <guibutton>Close</guibutton> to switch the primary mouse button from the left to the right (making the mouse suitable for use in the left hand)."
msgstr "मुख्य मेन्युपट्टीपासून <application>माऊस पसंती</application> सुरू करण्यासाठी <menuchoice><guimenu>प्रणाली</guimenu><guisubmenu>पसंती</guisubmenu><guimenuitem>माऊस</guimenuitem></menuchoice> निवडा. <guilabel>बटन्स</guilabel> टॅबमध्ये, <guilabel>डावखोरा माऊस</guilabel> चेकबॉक्स निवडा व प्राथमीक माऊस बटन डावीकडून उजवीकडे वापरण्याकरिता <guibutton>बंद करा</guibutton> क्लिक करा (डाव्या हाताने माऊसचा वापर करणाऱ्यांना फायदेशीर ठरते)."

# translation auto-copied from project Publican Common, version 3, document Conventions, author sandeeps
msgid "To insert a special character into a <application>gedit</application> file, choose <menuchoice><guimenu>Applications</guimenu><guisubmenu>Accessories</guisubmenu><guimenuitem>Character Map</guimenuitem></menuchoice> from the main menu bar. Next, choose <menuchoice><guimenu>Search</guimenu><guimenuitem>Find…</guimenuitem></menuchoice> from the <application>Character Map</application> menu bar, type the name of the character in the <guilabel>Search</guilabel> field and click <guibutton>Next</guibutton>. The character you sought will be highlighted in the <guilabel>Character Table</guilabel>. Double-click this highlighted character to place it in the <guilabel>Text to copy</guilabel> field and then click the <guibutton>Copy</guibutton> button. Now switch back to your document and choose <menuchoice><guimenu>Edit</guimenu><guimenuitem>Paste</guimenuitem></menuchoice> from the <application>gedit</application> menu bar."
msgstr "<application>जिएडिट</application> फाइलमध्ये विशेष अक्षर अंतर्भुत करण्यासाठी, मुख्य मेन्यु पट्टीतून <menuchoice><guimenu>ॲप्लिकेशन्स्</guimenu><guisubmenu>ॲसेसरिज्</guisubmenu><guimenuitem>कॅरेक्टर मॅप</guimenuitem></menuchoice> पसंत करा. पुढे, <menuchoice><guimenu>शोधा</guimenu><guimenuitem>शोधा…</guimenuitem></menuchoice> पसंत करा, <application>कॅरेक्टर मॅप</application> मेन्यु पट्टीतून, <guilabel>शोधा</guilabel> क्षेत्रात अक्षराचे नाव टाइप करा व <guibutton>पुढे</guibutton> बटन क्लिक करा. प्राप्य अक्षराला <guilabel>कॅरेक्टर टेबल</guilabel> येथे ठळक केले जाईल. <guilabel>प्रत बनवण्याजोगी मजकूर</guilabel> क्षेत्रात ह्या ठळक अक्षराला स्थित करण्यासाठी दोनवेळा क्लिक करा व त्यानंतर <guibutton>प्रत बनवा</guibutton> बटनावर क्लिक करा. आता दस्तऐवजांचा वापर करा व <menuchoice><guimenu>संपादित करा</guimenu><guimenuitem>चिकटवा</guimenuitem></menuchoice> पसंत करा, <application>जिएडिट</application> मेन्यु पट्टीतून."

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "The above text includes application names; system-wide menu names and items; application-specific menu names; and buttons and text found within a GUI interface, all presented in proportional bold and all distinguishable by context."
msgstr "वरील मजकूरमध्ये ऍप्लिकेशनचे नावे; प्रणाली-क्षेत्र मेन्यू नावे व घटके; ऍप्लिकेशन्स्-निर्देशीत मेन्यू नावे; व बटने व GUI संवादमधील मजकूर समाविष्टीत आहे, सर्व प्रप्रोर्श्नल बोल्डमध्ये प्रस्तुत केले आहेत व संदर्भशी नीगडीत आहे."

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "<command><replaceable>Mono-spaced Bold Italic</replaceable></command> or <application><replaceable>Proportional Bold Italic</replaceable></application>"
msgstr "<command><replaceable>मोनो-स्पेस्ड् बोल्ड इटॅलीक</replaceable></command> किंवा <application><replaceable>प्रप्रोर्श्नल बोल्ड इटॅलीक</replaceable></application>"

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "Whether mono-spaced bold or proportional bold, the addition of italics indicates replaceable or variable text. Italics denotes text you do not input literally or displayed text that changes depending on circumstance. For example:"
msgstr "मोनो-स्पेस्ड् बोल्ड किंवा प्रप्रोर्श्नल बोल्ड, इटॅलीक्स् बदलविण्याजोगी किंवा बदलाव पाठ्य दर्शविते. इटॅलीक्स् तुम्ही इच्छिकरित्या प्रविष्ट न केलेलेकिंवा परस्पर स्थिती नुरूप बदलणारे पाठ्य दर्शविते. उदाहरणार्थ:"

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "To connect to a remote machine using ssh, type <command>ssh <replaceable>username</replaceable>@<replaceable>domain.name</replaceable></command> at a shell prompt. If the remote machine is <filename>example.com</filename> and your username on that machine is john, type <command>ssh john@example.com</command>."
msgstr "दूर्रस्थ मशीनशी ssh द्वारे जुळवणी स्थापीत करायचे असल्यास, शेल प्रॉम्पटवर <command>ssh <replaceable>वापरकर्तानाव</replaceable>@<replaceable>क्षेत्र.नाव</replaceable></command> टाइप करा. दूर्रस्थ मशीन <filename>example.com</filename> असल्यास व वापरकर्त्याचे नाव john असल्यास, <command>ssh john@example.com</command> टाइप करा."

# translation auto-copied from project Publican Common, version 3, document
# Conventions
msgid "The <command>mount -o remount <replaceable>file-system</replaceable></command> command remounts the named file system. For example, to remount the <filename>/home</filename> file system, the command is <command>mount -o remount /home</command>."
msgstr "<command>mount -o remount <replaceable>फाइल-प्रणली</replaceable></command> आदेश, नामांकीत फाइल प्रणालीस पुन्ह आरोहीत करते. उदाहरणार्थ, <filename>/home</filename> फाइल प्रणाली पुन्हा माउंट करण्याकरिता, <command>mount -o remount /home</command> आदेशचा वापर करा."

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "To see the version of a currently installed package, use the <command>rpm -q <replaceable>package</replaceable></command> command. It will return a result as follows: <command><replaceable>package-version-release</replaceable></command>."
msgstr "वर्तमानरित्या प्रतिष्ठापीत संकुलाची आवृत्ती पहाची असल्यावर, <command>rpm -q <replaceable>संकुल</replaceable></command> आदेश वापरा. परिणाम खालिल नुरूप दर्शविले जातील: <command><replaceable>संकुल-आवृत्ती-प्रकाशन</replaceable></command>."

# translation auto-copied from project Publican Common, version 3, document Conventions, author sandeeps
msgid "Note the words in bold italics above: username, domain.name, file-system, package, version and release. Each word is a placeholder, either for text you enter when issuing a command or for text displayed by the system."
msgstr "वरील बोल्ड इटॅलिक्स् शब्दांकडे लक्ष द्या — वापरकर्तानाव, domain.name, फाइल-प्रणाली, संकुल, आवृत्ती व प्रकाशन. प्रत्येक शब्द प्लेसफोल्ड आहे, एकतर आदेश देतेवेळी दिलेल्या मजकूरकरीता किंवा प्रणालीतर्फे दाखवलेल्या मजकूरकरीता."

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "Aside from standard usage for presenting the title of a work, italics denotes the first use of a new and important term. For example:"
msgstr "कार्य शिर्षक करीता मानक वापर याच्या व्यतिरिक्त, इटॅलीक्स्चा वापरप्रथम किंवा महत्वाची संज्ञा दर्शविण्याकरीता केला जातो. उदाहरणार्थ:"

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "Publican is a <firstterm>DocBook</firstterm> publishing system."
msgstr "पब्लिकन <firstterm>डॉकबूक</firstterm> प्रकाशन प्रणाली आहे."

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "Pull-quote Conventions"
msgstr "पूल्ल-कोट् नियमावली"

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "Terminal output and source code listings are set off visually from the surrounding text."
msgstr "भोवतालच्या मजकूर पासून टर्मिनलचे आऊटपुट व स्रोत कोड सूची दृष्यास्पदपणे सेट केले जाते."

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "Output sent to a terminal is set in <computeroutput>mono-spaced roman</computeroutput> and presented thus:"
msgstr "टर्मिनलकरीता पाठवलेले आऊटपुट <computeroutput>मोनो-स्पेस्ड् रोमन</computeroutput> मध्ये निश्चित केले असते व याप्रमाणे प्रस्तुत केले जाते:"

# translation auto-copied from project Publican Common, version 3, document Conventions, author sandeeps
msgid "books        Desktop   documentation  drafts  mss    photos   stuff  svn\n"
"books_tests  Desktop1  downloads      images  notes  scripts  svgs"
msgstr "पुस्ते        डेस्कटॉप   दस्तऐवजीकरण  मसुदा  mss    फोटोज   स्टफ्  svn\n"
"books_tests  डेस्कटॉप1  डाऊनलोड्स्      प्रतिमा  नोट्स्  स्क्रिप्ट्स्  svgs"

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "Source-code listings are also set in <computeroutput>mono-spaced roman</computeroutput> but add syntax highlighting as follows:"
msgstr "सोअर्स-कोड सूची देखील <computeroutput>मोनो-स्पेस्ड् रोमन</computeroutput> मध्ये सेट केले जाते परंतु रचना या प्रकारे ठळक केली जाते:"

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "Notes and Warnings"
msgstr "टिप व सावधानता"

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "Finally, we use three visual styles to draw attention to information that might otherwise be overlooked."
msgstr "शेवटी, दुर्लक्ष करण्याजोगी माहितीकडे लक्ष वेधण्याकरीता तीन दृश्यास्पद शैली वापरली जाते."

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "Notes are tips, shortcuts or alternative approaches to the task at hand. Ignoring a note should have no negative consequences, but you might miss out on a trick that makes your life easier."
msgstr "टिपण्णी सहसा टिपा, शार्टकट्स् किंवा ठराविक कार्यसाठी वैकल्पिक पर्याय आहे. टिपांकडे दुर्लक्ष केल्यास नकारात्मक परिणाम आढळत नाही, परंतु बरेच सोप्या गोष्टी तुम्ही गमवाल."

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "Important boxes detail things that are easily missed: configuration changes that only apply to the current session, or services that need restarting before an update will apply. Ignoring a box labeled “Important” will not cause data loss but may cause irritation and frustration."
msgstr "महत्वाच्या बॉक्सेजमध्ये सहज न आढळणाऱ्या बाबचे समावेश आहे: संरचना बदल जे फक्त सध्याच्या सत्रकरीता लागू होतात, किंवा सेवा ज्यांस सुधारणापूर्वी पुनः सुरू करणे आवश्यक आहे लागू केले जातात. 'महत्वाचे' असे लेबल केलेल्या बॉक्सकडे दुर्लक्ष केल्यास डाटा नाहीसा होत नाही परंतु त्रास नक्की होतो."

# translation auto-copied from project Publican Common, version 3, document Conventions
msgid "Warnings should not be ignored. Ignoring warnings will most likely cause data loss."
msgstr "सावधानता दुर्लक्ष करू नका. सावधानताकडे दुर्लक्ष केल्यास माहिती लुप्त होऊ शकते."

