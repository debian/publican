# AUTHOR <EMAIL@ADDRESS>, YEAR.
# mgiri <mgiri@fedoraproject.org>, 2012. #zanata
msgid ""
msgstr ""
"Project-Id-Version: 0\n"
"POT-Creation-Date: 2013-11-13 16:27+1000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2012-09-03 09:20-0400\n"
"Last-Translator: mgiri <mgiri@fedoraproject.org>\n"
"Language-Team: None\n"
"Language: or\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"X-Generator: Zanata 3.0.3\n"

# translation auto-copied from project Publican Common, version 3, document Revision_History
#. Tag: title
#, no-c-format
msgid "Revision History"
msgstr "ସଂଶୋଧିତ ବିବରଣୀ"

# translation auto-copied from project Publican Common, version 3, document Revision_History, author mgiri
#. Tag: firstname
#, no-c-format
msgid "Jeff"
msgstr "ଜେଫ"

# translation auto-copied from project Publican Common, version 3, document Revision_History, author mgiri
#. Tag: surname
#, no-c-format
msgid "Fearn"
msgstr "ଫିୟର୍ଣ୍ଣ"

# translation auto-copied from project Publican Common, version 3, document Revision_History, author mgiri
#. Tag: member
#, no-c-format
msgid "Publican 3.0"
msgstr "Publican 3.0"

