# Gé Baylard <<Geodebay@gmail.com>>, 2013.
# Gé Baylard <geodebay@gmail.com>, 2013.
# Gé Baylard <geodebay@gmail.com>, 2014. #zanata
msgid ""
msgstr ""
"Project-Id-Version: 0\n"
"POT-Creation-Date: 2014-10-03 13:17+1000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2014-05-12 12:56-0400\n"
"Last-Translator: Gé Baylard <geodebay@gmail.com>\n"
"Language-Team: français <>\n"
"Language: fr\n"
"X-Generator: Zanata 3.6.2\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

msgid "Entities and translation"
msgstr "Entités et traductions"

msgid "Use entities with extreme caution"
msgstr "Être particulièrement précautionneux avec les entités"

msgid "Entities offer convenience but they should be used with extreme caution in documents that will be translated. Writing (for example) <tag>&amp;FDS;</tag> instead of <application>Fedora Directory Server</application> saves the writer time but transformed entities do not appear in the <firstterm>portable object</firstterm> (PO) files that translators use. Complete translations of documents containing entities are, as a consequence, impossible."
msgstr "Les entités offrent des commodités, mais elles doivent être utilisées avec beaucoup de discernement dans les documents à traduire. Écrire, par exemple, <sgmltag>&amp;FDS&semi;</sgmltag> au lieu de <application>Fedora Directory Server</application> économise le temps de celui qui écrit, mais les entités transformées n'apparaîtront pas dans les fichiers <firstterm>« portable object »</firstterm> (PO) utilisés par le traducteur. Les traductions complètes de documents contenant des entités sont, en conséquence, impossibles."

msgid "Entities present special obstacles to translators and can preclude the production of high-quality translations. The very nature of an entity is that the word or phrase represented by the entity is rendered exactly the same way every time that it occurs in the document, in every language. This inflexibility means that the word or word group represented by the entity might be illegible or incomprehensible in the target language and that the word or word group represented by the entity cannot change when the grammatical rules of the target language require them to change. Furthermore, because entities are not transformed when XML is converted to PO, translators cannot select the correct words that surround the entity, as required by the grammatical rules of the target language."
msgstr "Les entités sont des obstacles spécialement pour les traducteurs en empêchant la production de traductions de bonne qualité. Intrinsèquement, un mot ou une phrase représentés par une entité sont rendus de la même façon chaque fois que l'entité apparaît dans le document, quelle que soit la langue. Il résulte de cette rigidité que le mot, ou le groupe de mots, représenté par l'entité peut devenir illisible ou incompréhensible dans la langue cible ; de même, ce mot ou ce groupe de mots ne peut pas suivre les variations résultant de l'application de règles grammaticales pour la cible. Plus, comme les entités ne sont pas transformées lorsque le XML est converti en PO, les traducteurs ne peuvent pas choisir des termes corrects dans le contexte, comme l'exigeraient les règles grammaticales de la langue cible."

msgid "If you define an entity — <tag>&lt;!ENTITY LIFT \"Liberty Installation and Formatting Tome\"&gt;</tag> — you can enter <literal>&amp;LIFT;</literal> in your XML and it will appear as <literal>Liberty Installation and Formatting Tome</literal> every time the book is built as HTML, PDF or text."
msgstr "Si vous définissez une entité — <sgmltag>&lt;!ENTITY LIFT \"Liberty Installation and Formatting Tome\"&gt;</sgmltag> — vous pouvez saisir, à la place, <literal>&amp;LIFT&semi;</literal> dans le XML ; l'expression sera développée en <literal>Liberty Installation and Formatting Tome</literal> chaque fois que l'ouvrage sera construit au format HTML, PDF ou texte."

msgid "Entities are not transformed when XML is converted to PO, however. Consequently, translators never see <literal>Liberty Installation and Formatting Tome</literal>. Instead they see <literal>&amp;LIFT;</literal>, which they cannot translate."
msgstr "Les entités n'étant pas transformées quand le XML est converti en PO, les traducteurs, en conséquence, ne voient jamais <literal>Liberty Installation and Formatting Tome</literal>. Au lieu de cela, ils voient <literal>&amp;LIFT&semi;</literal>, qu'il leur est impossible de traduire."

msgid "Consider something as simple as the following English sentence fragment being translated into a related language: German."
msgstr "Examinons quelque chose d'aussi banal que le morceau suivant de phrase en anglais à traduire dans une langue de la même famille : l'allemand."

msgid "As noted in the <citetitle>Liberty Installation and Formatting Tome</citetitle>, Chapter 3…"
msgstr "As noted in the <citetitle>Liberty Installation and Formatting Tome</citetitle>, Chapter 3&hellip;"

msgid "A translation of this might be as follows:"
msgstr "Une traduction possible serait :"

msgid "<foreignphrase>Wie in dem <citetitle>Wälzer für die Installation und Formatierung von Liberty</citetitle>, Kapitel 3, erwähnt…</foreignphrase>"
msgstr "<foreignphrase>Wie in dem <citetitle>Wälzer für die Installation und Formatierung von Liberty</citetitle>, Kapitel 3, erwähnt&hellip;</foreignphrase>"

msgid "Because there is no text missing, the title can be translated into elegant German. Also, note the use of <foreignphrase>‘dem’</foreignphrase>, the correct form of the definite article ('the') when referring to a <foreignphrase>Wälzer</foreignphrase> ('tome') in this grammatical context."
msgstr "Comme l'intégralité du texte est disponible, le titre a pu être élégamment traduit en allemand. Notez l'utilisation de <foreignphrase>&lsquo;dem&rsquo;</foreignphrase>, forme correcte de l'article défini (« le ») se rapportant à <foreignphrase>Wälzer</foreignphrase> (« tome ») dans son contexte grammatical."

msgid "By contrast, if entities are used, the entry in the PO file says:"
msgstr "A contrario, si des entités ont été utilisées, l'entrée dans le fichier PO est ainsi rédigée :"

msgid ""
"#. Tag: para\n"
"#, no-c-format\n"
"msgid \"As noted in the &lt;citetitle&gt;&amp;LIFT;&lt;/citetitle&gt;, Chapter 3…\"\n"
"msgstr \"\""
msgstr ""
"\n"
"#. Tag: para\n"
"#, no-c-format\n"
"msgid \"As noted in the &lt;citetitle&gt;&amp;LIFT&semi;&lt;/citetitle&gt;, Chapter 3&hellip;\"\n"
"msgstr \"\"\n"
"\n"

msgid "The translation of this would probably run thus:"
msgstr "La traduction donnerait alors probablement :"

msgid ""
"#. Tag: para\n"
"#, no-c-format\n"
"msgid \"As noted in the &lt;citetitle&gt;&amp;LIFT;&lt;/citetitle&gt;, Chapter 3…\"\n"
"msgstr \"Wie in &lt;citetitle&gt;&amp;LIFT;&lt;/citetitle&gt;, Kapitel 3, erwähnt…\""
msgstr ""
"\n"
"#. Tag: para\n"
"#, no-c-format\n"
"msgid \"As noted in the &lt;citetitle&gt;&amp;LIFT&semi;&lt;/citetitle&gt;, Chapter 3&hellip;\"\n"
"msgstr \"Wie in &lt;citetitle&gt;&amp;LIFT&semi;&lt;/citetitle&gt;, Kapitel 3, erwähnt&hellip;\"\n"
"\n"

msgid "And the presentation would be thus:"
msgstr "Et le rendu serait :"

msgid "<foreignphrase>Wie in <citetitle>Liberty Installation and Formatting Tome</citetitle>, Kapitel 3, erwähnt…</foreignphrase>"
msgstr "<foreignphrase>Wie in <citetitle>Liberty Installation and Formatting Tome</citetitle>, Kapitel 3, erwähnt&hellip;</foreignphrase>"

msgid "This, of course, leaves the title in English, including words like 'Tome' and 'Formatting' that readers are unlikely to understand. Also, the translator is forced to omit the definite article ‘dem’, a more general construction that comes close to a hybrid of English and German that German speakers call <foreignphrase>Denglisch</foreignphrase> or <foreignphrase>Angleutsch</foreignphrase>. Many German speakers consider this approach incorrect and almost all consider it inelegant."
msgstr "Ceci, bien entendu, laisse le titre en anglais, avec des mots comme « Tome » et « Formatting » que des lecteurs pourraient ne pas comprendre. De même, le traducteur est forcé d'omettre l'article défini &lsquo;dem&rsquo;, construction générale s'approchant d'une hybridation anglais et allemand que les locuteurs allemands appellent <foreignphrase>Denglisch</foreignphrase> ou <foreignphrase>Angleutsch</foreignphrase>. Beaucoup de locuteurs allemands considèrent cette approche comme incorrecte et pratiquement tous la trouvent inélégante."

msgid "Equivalent problems emerge with a fragment such as this:"
msgstr "Des problèmes semblables sont constatés avec un fragment comme :"

msgid "However, a careful reading of the <citetitle>Liberty Installation and Formatting Tome</citetitle> afterword shows that…"
msgstr "However, a careful reading of the <citetitle>Liberty Installation and Formatting Tome</citetitle> afterword shows that&hellip;"

msgid "With no text hidden behind an entity, a German translation of this might be:"
msgstr "S'il n'y a pas de texte dissimulé dans une entité, une traduction possible en allemand serait :"

msgid "<foreignphrase>Jedoch ergibt ein sorgfältiges Lesen des Nachworts für den <citetitle>Wälzer für die Installation und Formatierung von Liberty</citetitle>, dass…</foreignphrase>"
msgstr "<foreignphrase>Jedoch ergibt ein sorgfältiges Lesen des Nachworts für den <citetitle>Wälzer für die Installation und Formatierung von Liberty</citetitle>, dass&hellip;</foreignphrase>"

msgid "If an entity was used to save the writer time, the translator has to deal with this:"
msgstr "Si une entité avait été employée pour que le rédacteur économise un peu de temps, le traducteur aurait été confronté à :"

msgid ""
"#. Tag: para\n"
"#, no-c-format\n"
"msgid \"However, a careful reading of the &lt;citetitle&gt;&amp;LIFT;&lt;/citetitle&gt; afterword shows that…\"\n"
"msgstr \"\""
msgstr ""
"\n"
"#. Tag: para\n"
"#, no-c-format\n"
"msgid \"However, a careful reading of the &lt;citetitle&gt;&amp;LIFT&semi;&lt;/citetitle&gt; afterword shows that&hellip;\"\n"
"msgstr \"\"\n"
"\n"

msgid "And the translation would be subtly but importantly different:"
msgstr "Et la traduction aurait présenté des différences sournoises, mais importantes :"

msgid ""
"#. Tag: para\n"
"#, no-c-format\n"
"msgid \"However, a careful reading of the &lt;citetitle&gt;&amp;LIFT;&lt;/citetitle&gt; afterword shows that…\"\n"
"msgstr \"Jedoch ergibt ein sorgfältiges Lesen des Nachworts für &lt;citetitle&gt;&amp;LIFT;&lt;/citetitle&gt;, dass…\""
msgstr ""
"\n"
"#. Tag: para\n"
"#, no-c-format\n"
"msgid \"However, a careful reading of the &lt;citetitle&gt;&amp;LIFT&semi;&lt;/citetitle&gt; afterword shows that&hellip;\"\n"
"msgstr \"Jedoch ergibt ein sorgfältiges Lesen des Nachworts für &lt;citetitle&gt;&amp;LIFT&semi;&lt;/citetitle&gt;, dass&hellip;\"\n"
"\n"

msgid "When presented to a reader, this would appear as follows:"
msgstr "Présenté au lecteur, cela donne :"

msgid "<foreignphrase>Jedoch ergibt ein sorgfältiges Lesen des Nachworts für <citetitle>Liberty Installation and Formatting Tome</citetitle>, dass…</foreignphrase>"
msgstr "<foreignphrase>Jedoch ergibt ein sorgfältiges Lesen des Nachworts für <citetitle>Liberty Installation and Formatting Tome</citetitle>, dass&hellip;</foreignphrase>"

msgid "Again, note the missing definite article (<foreignphrase>den</foreignphrase> in this grammatical context). This is inelegant but necessary since the translator can otherwise only guess which form of the definite article (<foreignphrase>den</foreignphrase>, <foreignphrase>die</foreignphrase> or <foreignphrase>das</foreignphrase>) to use, which would inevitably lead to error."
msgstr "À nouveau, notez l'absence de l'article défini (<foreignphrase>den</foreignphrase> dans ce contexte grammatical). Ce n'est pas élégant, mais imparable car le traducteur ne peut pas deviner quelle forme de l'article défini utiliser (<foreignphrase>den</foreignphrase>, <foreignphrase>die</foreignphrase> ou <foreignphrase>das</foreignphrase>) ; cela conduit inévitablement à une erreur."

msgid "Finally, consider that although a particular word never changes its form in English, this is not necessarily true of other languages, even when the word is a <firstterm>proper noun</firstterm> such as the name of a product. In many languages, nouns change (<firstterm>inflect</firstterm>) their form according to their role in a sentence (their grammatical <firstterm>case</firstterm>). An XML entity set to represent an English noun or noun phrase therefore makes correct translation impossible in such languages."
msgstr "Pour terminer, sachez que, même si un mot donné ne change jamais de forme en anglais, cela n'est pas nécessairement vrai dans d'autres langues, même quand le nom est un <firstterm>nom propre</firstterm>, comme le nom d'un produit. Dans beaucoup de langues, les termes changent de forme (<firstterm>présentent une flexion</firstterm>) selon leur rôle dans la phrase (leur <firstterm>cas</firstterm> grammatical). Une entité XML définie pour représenter un nom ou une phrase en anglais peut rendre une traduction correcte impossible dans de telles langues."

msgid "For example, if you write a document that could apply to more than one product, you might be tempted to set an entity such as <tag>&amp;PRODUCT;</tag>. The advantage of this approach is that by simply changing this value in the <filename><replaceable>Doc_Name</replaceable>.ent</filename> file, you could easily adjust the book to document (for example) Red Hat Enterprise Linux, Fedora, or CentOS. However, while the proper noun <literal>Fedora</literal> never varies in English, it has multiple forms in other languages. For example, in Czech the name <literal>Fedora</literal> has six different forms, depending on one of seven ways in which you can use it in a sentence:"
msgstr "Par exemple, si vous écrivez un document qui s'applique à plusieurs produits, vous pouvez être tenté d'utiliser une entité comme <sgmltag>&amp;PRODUCT&semi;</sgmltag>. L'avantage de cette approche est qu'en changeant simplement sa valeur dans le fichier <filename><replaceable>Nom_document</replaceable>.ent</filename>, vous pourrez facilement adapter l'ouvrage au document (par exemple) Red Hat Enterprise Linux, Fedora ou CentOS. Toutefois, alors que le nom propre <literal>Fedora</literal> ne change jamais en anglais, il a plusieurs formes dans d'autres langues. Par exemple, en tchèque, le nom <literal>Fedora</literal> a six formes différentes, selon l'une des sept possibilités d'utilisation dans une phrase :"

msgid "'Fedora' in Czech"
msgstr "« Fedora » en tchèque"

msgid "Case"
msgstr "Cas"

msgid "Usage"
msgstr "Utilisation"

msgid "Form"
msgstr "Forme"

msgid "Nominative"
msgstr "Nominatif"

msgid "the subject of a sentence"
msgstr "le sujet du verbe"

msgid "<foreignphrase>Fedora</foreignphrase>"
msgstr "<foreignphrase>Fedora</foreignphrase>"

msgid "Genitive"
msgstr "Génitif"

msgid "indicates possession"
msgstr "indique la possession"

msgid "<foreignphrase>Fedory</foreignphrase>"
msgstr "<foreignphrase>Fedory</foreignphrase>"

msgid "Accusative"
msgstr "Accusatif"

msgid "the direct object of a sentence"
msgstr "complément d'objet direct"

msgid "<foreignphrase>Fedoru</foreignphrase>"
msgstr "<foreignphrase>Fedoru</foreignphrase>"

msgid "Dative"
msgstr "Datif"

msgid "the indirect object of a sentence"
msgstr "complément d'objet indirect"

msgid "<foreignphrase>Fedoře</foreignphrase>"
msgstr "<foreignphrase>Fedoře</foreignphrase>"

msgid "Vocative"
msgstr "Vocatif"

msgid "the subject of direct address"
msgstr "appel direct du sujet"

msgid "<foreignphrase>Fedoro</foreignphrase>"
msgstr "<foreignphrase>Fedoro</foreignphrase>"

msgid "Locative"
msgstr "Locatif"

msgid "relates to a location"
msgstr "relatif à un emplacement"

msgid "Instrumental"
msgstr "Instrumental"

msgid "relates to a method"
msgstr "relatif à une méthode"

msgid "<foreignphrase>Fedorou</foreignphrase>"
msgstr "<foreignphrase>Fedorou</foreignphrase>"

msgid "For example:"
msgstr "Par exemple :"

msgid "<foreignphrase>Fedora je linuxová distribuce.</foreignphrase> — Fedora is a Linux distribution."
msgstr "<foreignphrase>Fedora je linuxová distribuce.</foreignphrase> — Fedora est une distribution Linux."

msgid "<foreignphrase>Inštalácia Fedory</foreignphrase> — Installation of Fedora"
msgstr "<foreignphrase>Inštalácia Fedory</foreignphrase> — installation de Fedora"

msgid "<foreignphrase>Stáhnout Fedoru</foreignphrase> — Get Fedora"
msgstr "<foreignphrase>Stáhnout Fedoru</foreignphrase> — obtenez Fedora"

msgid "<foreignphrase>Přispějte Fedoře</foreignphrase> — Contribute to Fedora"
msgstr "<foreignphrase>Přispějte Fedoře</foreignphrase> — contribuez à Fedora"

msgid "<foreignphrase>Ahoj, Fedoro!</foreignphrase> — Hello Fedora!"
msgstr "<foreignphrase>Ahoj, Fedoro!</foreignphrase> — Hello Fedora !"

msgid "<foreignphrase>Ve Fedoře 10…</foreignphrase> — In Fedora 10…"
msgstr "<foreignphrase>Ve Fedoře 10&hellip;</foreignphrase> — dans Fedora 10&hellip;"

msgid "<foreignphrase>S Fedorou získáváte nejnovější…</foreignphrase> — With Fedora, you get the latest…"
msgstr "<foreignphrase>S Fedorou získáváte nejnovější&hellip;</foreignphrase> — avec Fedora, vous obtenez la dernière &hellip;"

msgid "A sentence that begins <foreignphrase>S Fedora získáváte nejnovější…</foreignphrase> remains comprehensible to Czech readers, but the result is not grammatically correct. The same effect can be simulated in English, because although English nouns lost their case endings during the Middle Ages, English pronouns are still inflected. The sentence, 'Me see she' is completely comprehensible to English speakers, but is not what they expect to read, because the form of the pronouns <literal>me</literal> and <literal>she</literal> is not correct. <literal>Me</literal> is the accusative form of the pronoun, but because it is the subject of the sentence, the pronoun should take the nominative form, <literal>I</literal>. Similarly, <literal>she</literal> is nominative case, but as the direct object of the sentence the pronoun should take its accusative form, <literal>her</literal>."
msgstr "Une phrase qui commence par <foreignphrase>S Fedora získáváte nejnovější&hellip;</foreignphrase> reste compréhensible au lecteur tchèque, mais le résultat n'est pas grammaticalement correct. Le même effet peut être simulé en anglais, car même si les mots anglais ont perdu leurs déclinaisons au cours du Moyen Âge, les pronoms anglais sont restés fléchis. La phrase, « Me see she » est tout à fait compréhensible pour les locuteurs anglais, mais ce n'est pas ce à quoi ils s'attendent, parce que la forme des pronoms <literal>me</literal> et <literal>she</literal> n'est pas correcte. <literal>Me</literal> est l'accusatif du pronom, et comme il est le sujet de la phrase, le pronom aurait dû être au nominatif, <literal>I</literal>. De même, <literal>she</literal> est au nominatif, mais comme complément direct d'objet dans la phrase il aurait dû être à l'accusatif, <literal>her</literal>."

msgid "Nouns in most Slavic languages like Russian, Ukrainian, Czech, Polish, Serbian, and Croatian have seven different cases. Nouns in Finno–Ugaric languages such as Finnish, Hungarian, and Estonian have between fifteen and seventeen cases. Other languages alter nouns for other reasons. For example, Scandinavian languages inflect nouns to indicate <firstterm>definiteness</firstterm> — whether the noun refers to '<emphasis>a</emphasis> thing' or '<emphasis>the</emphasis> thing' — and some dialects of those languages inflect nouns both for definiteness <emphasis>and</emphasis> for grammatical case."
msgstr "Les noms dans la plupart des langues slaves comme le russe, l'ukrainien, le tchèque, le polonais, le serbe ou le croate ont sept cas différents. Les noms dans les langues finno-ougriennes comme le finnois, le hongrois ou l'este ont entre quinze et dix-sept cas. D'autres langues modifient les mots pour d'autres raisons. Par exemple, les langues scandinaves fléchissent les noms pour indiquer leur <firstterm>caractère défini</firstterm> ou pas — i.e. si le nom se rapporte à « <emphasis>une</emphasis> chose » ou à « <emphasis>la</emphasis> chose » — et quelques dialectes de ces langues fléchissent les noms à la fois selon leur caractère défini ou non <emphasis>et</emphasis> selon leur cas grammatical."

msgid "Now multiply such problems by the more than 40 languages that <application>Publican</application> currently supports. Other than the few non-translated strings that <application>Publican</application> specifies by default in the <filename><replaceable>Doc_Name</replaceable>.ent</filename> file, entities might prove useful for version numbers of products. Beyond that, the use of entities is tantamount to a conscious effort to inhibit and reduce the quality of translations. Furthermore, readers of your document in a language that inflects nouns (whether for case, definiteness, or other reasons) will not know that the bad grammar is the result of XML entities that you set — they will probably assume that the translator is incompetent."
msgstr "Maintenant, multipliez ces problèmes par 40, nombre de langues que <application>Publican</application> prend en charge actuellement. Mises à part les quelques chaînes non-traduites que <application>Publican</application> définit par défaut dans le fichier <filename><replaceable>Nom_document</replaceable>.ent</filename>, les entités peuvent trouver une utilité pour les numéros de version des produits. Exceptés ces cas, l'utilisation d'entités équivaut à s'efforcer de restreindre la qualité des traductions en toute connaissance de cause. De plus, les lecteurs du document dans les langues qui fléchissent les noms (que ce soit pour respecter le cas, le caractère défini ou autre raison) ignorent que cette mauvaise grammaire résulte de la définition des entités XML — ils vont probablement supposer que le traducteur est incompétent."

