# AUTHOR <EMAIL@ADDRESS>, YEAR.
# Gé Baylard <<Geodebay@gmail.com>>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: 0\n"
"POT-Creation-Date: 2014-10-03 13:17+1000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2014-05-14 11:50-0400\n"
"Last-Translator: Gé Baylard <<Geodebay@gmail.com>>\n"
"Language-Team: français <<trans-fr@lists.fedoraproject.org>>\n"
"Language: fr\n"
"X-Generator: Zanata 3.6.2\n"
"Plural-Forms: nplurals=2; plural=n>1;\n"

msgid "Docker container"
msgstr "Conteneur Docker"

msgid "This installation procedure assumes you have already installed a working docker (see <uri xlink:href=\"http://docker.io\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">http://docker.io</uri>) environment."
msgstr ""

msgid "Docker is a lightweight Jail (currently using LXC) that allows you to install and run <application>publican</application> without installing your main Linux installation with all its dependencies."
msgstr "Docker est une encapsulation légère (utilisant actuellement LXC) qui vous permet d'installer et exécuter <application>publican</application> sans modifier votre installation Linux principale et ses dépendances."

msgid "Open a terminal."
msgstr "Ouvrez un terminal."

msgid "Download and install the <application>svendowideit/publican</application> container from <uri xlink:href=\"https://index.docker.io/u/svendowideit/publican/\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">https://index.docker.io/u/svendowideit/publican/</uri>:"
msgstr ""

msgid "<prompt>$</prompt> <command>docker pull svendowideit/publican</command>"
msgstr "<prompt>$</prompt> <command>docker pull svendowideit/publican</command>"

msgid "This will take some time, as it downloads a fedora based container, and then the dependencies needed for publican"
msgstr "L'exécution de cette commande demandera un certain temps, car elle télécharge un conteneur fondé sur fedora, puis les dépendances nécessaires pour publican."

msgid "Add a publican bash alias to simplify your usage:"
msgstr "Ajoutez un alias de l'interpréteur bash de publican pour faciliter son utilisation :"

msgid "<prompt>$</prompt> <command>echo 'alias publican=\"docker run -t -i -v $(pwd):/mnt svendowideit/publican\"' &gt;&gt; ~/.bashrc</command>"
msgstr "<prompt>$</prompt> <command>echo 'alias publican=\"docker run -t -i -v $(pwd):/mnt svendowideit/publican\"' &gt;&gt; ~/.bashrc</command>"

msgid "This alias assumes that you are running <application>publican</application> in the documentation root directory (the one with the publican.cfg file in it."
msgstr "Cet alias suppose que vous faites tourner <application>publican</application> dans le répertoire racine de la documentation (celui contenant le fichier publican.cfg)."

msgid "now you can use <application>publican</application> as per the documentation:"
msgstr "Maintenant vous pouvez vous servir de <application>publican</application> conformément à la documentation du produit :"

msgid "<prompt>$</prompt> <command>publican --version </command>\n"
"<computeroutput>version=3.2.1</computeroutput>"
msgstr "<prompt>$</prompt> <command>publican --version </command>\n"
"<computeroutput>version=3.2.1</computeroutput>"

