# translation of Publican Users' Guide to Italiano
#
# Luigi Votta <lewis41@fedoraproject.org>, 2010.
# Mario Santagiuliana <fedora at marionline.it>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: 0\n"
"POT-Creation-Date: 2014-10-03 13:17+1000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2012-01-22 10:29-0500\n"
"Last-Translator: Mario Santagiuliana <fedora at marionline.it>\n"
"Language-Team: Italian <fedora-trans-it at redhat.com>\n"
"Language: it\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Zanata 3.6.2\n"

msgid "Discouraged elements and attributes"
msgstr "Elementi ed attributi non permessi"

msgid "Supported, unsupported, and discouraged"
msgstr "Supportato, non supportato e non permesso"

msgid "Not every <firstterm>element</firstterm> (tag) and attribute that works with <application>Publican</application> is <firstterm>supported</firstterm>. Specifically, not every tag has been tested with regards its effect on the presentation of a document once it has been built in HTML or PDF."
msgstr "Non tutti gli <firstterm>elementi</firstterm> (tag) ed attributi che funzionano con <application>Publican</application> sono supportati. Nello specifico, non tutti i tag sono stati testati riguardo agli effetti sulla presentazione di un documento in formato HTML o PDF."

msgid "<application>Publican</application> works with almost all DocBook elements and their attributes, and most of these elements are <firstterm>supported</firstterm>. Supported elements and attributes are those whose presentation in <application>Publican</application> HTML and PDF output has been tested and is of an acceptable quality."
msgstr "<application>Publican</application> funziona con quasi tutti gli elementi e relativi attributi di DocBook 4.5, la maggior parte dei quali sono <firstterm>supportati</firstterm>. Gli elementi ed attributi supportati sono quelli la cui presentazione in HTML e PDF di <application>Publican</application> sono stati testati e conservano un livello di qualità accettabile."

msgid "Other elements and attributes that are not known to be harmful or redundant but which have not been tested for quality are <firstterm>unsupported</firstterm>. If material within a particular DocBook tag does not look correct when you build a document in HTML or PDF, the problem could be that the transformation logic for that tag has not yet been tested. Build the document again and examine <application>Publican</application>'s output as the document builds. <application>Publican</application> presents warnings about unsupported tags that it encounters in your XML files."
msgstr "Altri elementi ed attributi non riconosciuti dannosi o ridondanti, ma che non sono stati testati per qualità sono <firstterm>non supportati</firstterm>. Se il contenuto all'interno di un particolare tag DocBook non viene visualizzato correttamente in un documento HTML o PDF, il problema può derivare da una mancanza di test sulla logica di trasformazione del relativo tag. Provare a ricreare il documento e controllare l'output generato da <application>Publican</application> durante la creazione del documento. <application>Publican</application> presenta avvisi sui tag non supportati man mano che vengono elaborati i file XML."

msgid "Finally, a small group of elements and attributes are <firstterm>discouraged</firstterm>. These elements and attributes are set out below, each accompanied by rationale explaining why it is discouraged."
msgstr "Infine, un ristretto gruppo di elementi ed attributi sono <firstterm>non permessi</firstterm>. Questi elementi ed attributi sono elencati in basso, accompagnati da una spiegazione del motivo del divieto."

msgid "Use the command <prompt>$</prompt> <command>publican print_known</command> to print a list of tags that <application>Publican</application> supports, and the command <command>publican print_banned</command> to print a list of tags that are banned in <application>Publican</application>."
msgstr "Usare il comando <command>publican print_known</command> per visualizzare l'elenco dei tag supportati da <application>Publican</application>, ed il comando <command>publican print_banned</command> per visualizzare l'elenco dei tag non permessi in <application>Publican</application>."

msgid "Discouraged elements"
msgstr ""

msgid "<tag>&lt;glossdiv&gt;</tag>"
msgstr "<sgmltag>&lt;glossdiv&gt;</sgmltag>"

msgid "This tag set presents terms in glossaries in alphabetical order; however, the terms are sorted according to the original language of the XML, regardless of how these terms are translated into any other language. For example, a glossary produced with <tag>&lt;glossdiv&gt;</tag>s that looks like this in English:"
msgstr "Questo tag presenta i termini nei glossari in ordine alfabetico; tuttavia, i termini vengono ordinati secondo la lingua originale dell'XML, a prescindere se i termini siano tradotti in altre lingue. Per esempio, un glossario prodotto con <sgmltag>&lt;glossdiv&gt;</sgmltag> che in lingua inglese appare come:"

msgid "A"
msgstr "A"

msgid "<literal>Apple</literal> — an <firstterm>apple</firstterm> is…"
msgstr "<literal>Apple</literal> — an <firstterm>apple</firstterm> is&hellip;"

msgid "G"
msgstr "G"

msgid "<literal>Grapes</literal> — <firstterm>grapes</firstterm> are…"
msgstr "<literal>Grapes</literal> — <firstterm>grapes</firstterm> are&hellip;"

msgid "O"
msgstr "O"

msgid "<literal>Orange</literal> — an <firstterm>orange</firstterm> is…"
msgstr "<literal>Orange</literal> — an <firstterm>orange</firstterm> is&hellip;"

msgid "P"
msgstr "P"

msgid "<literal>Peach</literal> — a <firstterm>peach</firstterm> is…"
msgstr "<literal>Peach</literal> — a <firstterm>peach</firstterm> is&hellip;"

msgid "looks like this in Spanish:"
msgstr "in italiano appare come:"

msgid "<literal>Manzana</literal> — la <firstterm>manzana</firstterm> es…"
msgstr "<literal>Mela</literal> — una <firstterm>mela</firstterm> è&hellip;"

msgid "<literal>Uva</literal> — la <firstterm>uva</firstterm> es…"
msgstr "<literal>Uva</literal> — l'<firstterm>uva</firstterm> è&hellip;"

msgid "<literal>Naranja</literal> — la <firstterm>naranja</firstterm> es…"
msgstr "<literal>Arancia</literal> — un'<firstterm>arancia</firstterm> è&hellip;"

msgid "<literal>Melocotonero</literal> — el <firstterm>melocotonero</firstterm> es…"
msgstr "<literal>Pera</literal> — la <firstterm>pera</firstterm> è&hellip;"

msgid "In a translated language that does not share the same writing system with the original language in which the XML was written, the result is even more nonsensical."
msgstr "Quindi tradotto in una lingua che non condivide lo stesso sistema di scrittura della lingua originale in cui è redatto l'XML, il risultato del tag è privo di senso."

msgid "<tag>&lt;inlinegraphic&gt;</tag>"
msgstr "<sgmltag>&lt;inlinegraphic&gt;</sgmltag>"

msgid "This element presents information as a graphic rather than as text and does not provide an option to present a text alternative to the graphic. This tag therefore hides information from people with visual impairments. In jurisdictions that have legal requirements for electronic content to be accessible to people with visual impairments, documents that use this tag will not satisfy those requirements. Section 508 of the <citetitle>Rehabilitation Act of 1973</citetitle><footnote> <para> Refer to <link xlink:href=\"http://www.section508.gov/\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">http://www.section508.gov/</link> </para> </footnote> is an example of such a requirement for federal agencies in the United States."
msgstr "Questo elemento presenta le informazioni contenute in un oggetto grafico, privo di una opzione per una presentazione alternativa in modalità testuale. Perciò questo tag limita l'accesso alle informazioni alle persone con ridotte capacità visive. Negli Stati in cui vige una legislazione che garantisce l'accessibilità ai contenuti elettronici alle persone con ridotte capacità visive, i documenti contenenti questo tag quindi non soddisfano queste richieste. La Section 508 del <citetitle>Rehabilitation Act of 1973</citetitle><footnote> <para> Fare riferimento a <ulink url=\"http://www.section508.gov/\">http://www.section508.gov/</ulink> </para> </footnote> è un esempio di tali richieste predisposte alle Agenzie Federali degli Stati Uniti d'America."

msgid "Note that <tag>&lt;inlinegraphic&gt;</tag> is not valid in DocBook version 5."
msgstr "Notare che il tag <sgmltag>&lt;inlinegraphic&gt;</sgmltag> non è valido in DocBook versione 5."

msgid "<tag>&lt;olink&gt;</tag>"
msgstr "<sgmltag>&lt;olink&gt;</sgmltag>"

msgid "The <tag>&lt;olink&gt;</tag> tag provides cross-references between XML documents. For <tag>&lt;olink&gt;</tag>s to work outside of documents that are all hosted within the same library of XML files, you must provide a URL for the document to which you are linking. In environments that use <tag>&lt;olink&gt;</tag>s, these URLs can be supplied either as an XML entity or with a server-side script. <application>Publican</application> does not provide any way to build or use a database for these links."
msgstr ""

msgid "Discouraged attributes"
msgstr "Attributi non permessi"

msgid "<tag>&lt;[element] xreflabel=\"[any_string_here]\"&gt;</tag>"
msgstr "<sgmltag>&lt;[element] xreflabel=\"[ogni_altra_stringa]\"&gt;</sgmltag>"

msgid "The presence of an <tag>&lt;xreflabel&gt;</tag> attribute reduces the usability of printed versions of a book. As well, attribute values are not seen by translators and, consequently, cannot be translated."
msgstr "La presenza di un attributo <sgmltag>&lt;xreflabel&gt;</sgmltag> riduce l'usabilità delle versioni stampate di un libro. Inoltre, i valori dell'attributo non sono visibili ai traduttori, e perciò non sono traducibili."

msgid "For example, if you have the following:"
msgstr "Per esempio, il seguente pezzo:"

#, fuzzy
msgid ""
"&lt;chapter id=\"ch03\" xreflabel=\"Chapter Three\"&gt;\n"
"	&lt;title&gt;The Secret to Eternal Life&lt;/title&gt;\n"
"	&lt;para&gt;The secret to eternal life is…&lt;/para&gt;\n"
"&lt;/chapter&gt;\n"
"\n"
"[more deathless prose here]     \n"
"\n"
"…see &lt;xref linkend=\"ch03\"&gt; for details."
msgstr ""
"\n"
"&lt;chapter id=\"ch03\" xreflabel=\"Chapter Three\"&gt;\n"
"\t&lt;title&gt;The Secret to Eternal Life&lt;/title&gt;\n"
"\t&lt;para&gt;The secret to eternal life is&hellip;&lt;/para&gt;\n"
"&lt;/chapter&gt;\n"
"\n"
"[more deathless prose here]     \n"
"\n"
"&hellip;see &lt;xref linkend=\"ch03\"&gt; for details.\n"
"\n"

msgid "when your XML is built to HTML, the <tag>&lt;xref&gt;</tag> tag becomes an HTML anchor tag as follows:"
msgstr "durante la trasformazione in HTML del file XML, il tag <sgmltag>&lt;xref&gt;</sgmltag> diventa un tag anchor, come indicato di seguito:"

msgid "…see &lt;a href=\"#ch03\"&gt;Chapter Three&lt;/a&gt; for details."
msgstr ""
"\n"
"&hellip;see &lt;a href=\"#ch03\"&gt;Chapter Three&lt;/a&gt; for details.\n"
"\n"

msgid "The text contained by the anchor tag is the same as the data in the <tag>&lt;xreflabel&gt;</tag> attribute. In this case, it means that readers of printed copies have less information available to them."
msgstr "Il testo contenuto nel tag anchor coincide con quello nell'attributo <sgmltag>&lt;xreflabel&gt;</sgmltag>. In questo caso, ciò vuol dire che i lettori delle versioni stampate hanno una perdita di informazione."

msgid "You could work around this if you make the value of the <tag>&lt;xreflabel&gt;</tag> attribute the same as the text within the <tag>&lt;title&gt;</tag><tag>&lt;/title&gt;</tag> element tags. However, this duplication increases the risk of typo-level errors and otherwise offers no underlying improvement. And it still reduces the amount of information presented to readers of printed copies."
msgstr "Per evitare questo problema si può far coincidere il valore dell'attributo <sgmltag>&lt;xreflabel&gt;</sgmltag> con il testo contenuto tra i tag <sgmltag>&lt;title&gt;</sgmltag><sgmltag>&lt;/title&gt;</sgmltag>. Tuttavia questa duplicazione aumenta il rischio di errori di battitura, senza in effetti alcun miglioramento. E riduce anche la quantità di informazione presentata ai lettori delle copie stampate."

msgid "The following XML:"
msgstr "Il seguente XML:"

#, fuzzy
msgid ""
"&lt;chapter id=\"ch03\" xreflabel=\"The Secret to Eternal Life\"&gt;\n"
"	&lt;title&gt;The Secret to Eternal Life&lt;/title&gt;\n"
"	&lt;para&gt;The secret to eternal life is…&lt;/para&gt;\n"
"&lt;/chapter&gt;\n"
"\n"
"[more deathless prose here]     \n"
"\n"
"…see &gt;xref linkend=\"ch03\"&gt; for details."
msgstr ""
"\n"
"&lt;chapter id=\"ch03\" xreflabel=\"The Secret to Eternal Life\"&gt;\n"
"\t&lt;title&gt;The Secret to Eternal Life&lt;/title&gt;\n"
"\t&lt;para&gt;The secret to eternal life is&hellip;&lt;/para&gt;\n"
"&lt;/chapter&gt;\n"
"\n"
"[more deathless prose here]     \n"
"\n"
"&hellip;see &gt;xref linkend=\"ch03\"&gt; for details.\n"
"\n"

msgid "Will result in an HTML anchor tag as follows:"
msgstr "viene trasformato in tag anchor HTML come segue:"

msgid "…see &lt;a href=\"#ch03\"&gt;The Secret to Eternal Life&lt;/a&gt; for details."
msgstr ""
"\n"
"&hellip;see &lt;a href=\"#ch03\"&gt;The Secret to Eternal Life&lt;/a&gt; for details.\n"
"\n"

msgid "This isn't as informative as the text presented to a reader if you do not use an <tag>&lt;xreflabel&gt;</tag> attribute. The following:"
msgstr "Ciò non è così informativo come il testo presentato senza usare l'attributo <sgmltag>&lt;xreflabel&gt;</sgmltag>. Il seguente:"

#, fuzzy
msgid ""
"&lt;chapter id=\"ch03\"&gt;\n"
"	&lt;title&gt;The Secret to Eternal Life&lt;/title&gt;\n"
"	&lt;para&gt;The secret to eternal life is…&lt;/para&gt;\n"
"&lt;/chapter&gt;\n"
"\n"
"[more deathless prose here]		\n"
"\n"
"…see &lt;xref linkend=\"ch03\"&gt; for details."
msgstr ""
"\n"
"&lt;chapter id=\"ch03\"&gt;\n"
"\t&lt;title&gt;The Secret to Eternal Life&lt;/title&gt;\n"
"\t&lt;para&gt;The secret to eternal life is&hellip;&lt;/para&gt;\n"
"&lt;/chapter&gt;\n"
"\n"
"[more deathless prose here]\t\t\n"
"\n"
"&hellip;see &lt;xref linkend=\"ch03\"&gt; for details.\n"
"\n"

msgid "transforms the <tag>&lt;xref&gt;</tag> element as follows when built to HTML:"
msgstr "trasforma l'elemento <sgmltag>&lt;xref&gt;</sgmltag> come segue in formato HTML:"

msgid "…see &lt;a href=\"#ch03\"&gt;Chapter 3: The Secret to Eternal Life&lt;/a&gt; for details."
msgstr ""
"\n"
"&hellip;see &lt;a href=\"#ch03\"&gt;Chapter 3: The Secret to Eternal Life&lt;/a&gt; for details.\n"
"\n"

msgid "More important, however, are the translation problems that <tag>&lt;xreflabel&gt;</tag> tags cause. Attribute values are not seen by translators. Consequently, they are not translated. Consider the second example above again:"
msgstr "Comunque, ancora più importanti, sono i problemi di traduzione causati dal tag <sgmltag>&lt;xreflabel&gt;</sgmltag>. I valori degli attributi non sono visibili ai traduttori. Di conseguenza, non possono essere tradotti. Si consideri di nuovo il secondo esempio, di cui sopra:"

#, fuzzy
msgid ""
"&lt;chapter id=\"ch03\" xreflabel=\"The Secret to Eternal Life\"&gt;\n"
"	&lt;title&gt;The Secret to Eternal Life&lt;/title&gt;\n"
"	&lt;para&gt;The secret to eternal life is…&lt;/para&gt;\n"
"&lt;/chapter&gt;\n"
"\n"
"[more deathless prose here]		\n"
"\n"
"…see &lt;xref linkend=\"ch03\"&gt; for details."
msgstr ""
"\n"
"&lt;chapter id=\"ch03\" xreflabel=\"The Secret to Eternal Life\"&gt;\n"
"\t&lt;title&gt;The Secret to Eternal Life&lt;/title&gt;\n"
"\t&lt;para&gt;The secret to eternal life is&hellip;&lt;/para&gt;\n"
"&lt;/chapter&gt;\n"
"\n"
"[more deathless prose here]\t\t\n"
"\n"
"&hellip;see &lt;xref linkend=\"ch03\"&gt; for details.\n"
"\n"

msgid "In English, the <tag>&lt;xref&gt;</tag> is still transformed into an anchor tag as follows:"
msgstr "In inglese, <sgmltag>&lt;xref&gt;</sgmltag> è ancora trasformato in un tag anchor come segue:"

msgid "Someone reading the German version, however, will have this as their underlying HTML:"
msgstr "Mentre i lettori della versione in lingua tedesca, visualizzeranno il seguente HTML:"

msgid "…Sehen Sie &lt;a href=\"#ch03\"&gt;The Secret to Eternal Life&lt;/a&gt; für Details."
msgstr ""
"\n"
"&hellip;Sehen Sie &lt;a href=\"#ch03\"&gt;The Secret to Eternal Life&lt;/a&gt; für Details.\n"
"\n"

msgid "If the <tag>&lt;xreflabel&gt;</tag> attribute is not used, the title and chapter indicator, both properly translated, appear to the reader. That is, the following:"
msgstr "Rimuovendo l'attributo <sgmltag>&lt;xreflabel&gt;</sgmltag>, i tag del titolo e del capitolo, appaiono propriamente tradotti al lettore. Cioè il seguente pezzo:"

msgid "will, after translation, present thus to a German-speaking reader:"
msgstr "per una traduzione in lingua tedesca, viene visualizzato come:"

msgid "…Sehen Sie &lt;a href=\"#ch03\"&gt;Kapitel 3: Das Geheimnis des ewigen Lebens&lt;/a&gt; für Details."
msgstr ""
"\n"
"&hellip;Sehen Sie &lt;a href=\"#ch03\"&gt;Kapitel 3: Das Geheimnis des ewigen Lebens&lt;/a&gt; für Details.\n"
"\n"

msgid "This is, not surprisingly, what we want."
msgstr "E ciò, senza meravigliarsi più di tanto, è ciò che si vuole ottenere!"

msgid "The <parameter>xreflabel</parameter> attribute is therefore discouraged."
msgstr "Quindi l'attributo <parameter>xreflabel</parameter> è vietato."

msgid "<tag>&lt;[element] endterm=\"[any_string_here]\"&gt;</tag>"
msgstr "<sgmltag>&lt;[element] endterm=\"[any_string_here]\"&gt;</sgmltag>"

msgid "The <parameter>endterm</parameter> attribute allows you to present hyperlinked text other than the name of the section or chapter to which the hyperlink points. As such, it decreases the usability of printed versions of documents, and causes difficulty for translators."
msgstr "L'attributo <parameter>endterm</parameter> permette di presentare il testo relativo all'ipertesto con un nome diverso dalla sezione o capitolo cui punta il collegamento. E ciò riduce l'usabilità della versione stampata del documento e genera difficoltà anche ai traduttori."

msgid "The text presented in an element (such as an <tag>&lt;xref&gt;</tag>) that contains the <parameter>endterm</parameter> attribute is taken from a <tag>&lt;titleabbrev&gt;</tag> tag in the target chapter or section. Although the content of the <tag>&lt;titleabbrev&gt;</tag> tag is available to translators in the document's PO files, it is removed from the context of the <tag>&lt;xref&gt;</tag>. The absence of this context makes reliable translation impossible in languages that mark prepositions or articles for grammatical number and grammatical gender."
msgstr "Il testo presentato in un elemento (come un <sgmltag>&lt;xref&gt;</sgmltag>), contenente l'attributo <parameter>endterm</parameter> è ricavato dal tag <sgmltag>&lt;titleabbrev&gt;</sgmltag> nel capitolo o sezione target. Sebbene il contenuto del tag <sgmltag>&lt;titleabbrev&gt;</sgmltag> sia traducibile nei file PO del documento, esso viene di fatto rimosso dal contesto del tag <sgmltag>&lt;xref&gt;</sgmltag>. L'assenza di questo contesto rende praticamente impossibile la traduzione di articoli e preposizioni, preservando genere e numero."

#, fuzzy
msgid ""
"&lt;chapter id=\"The_Secret\"&gt;\n"
"	&lt;title&gt;The Secret to Eternal Life&lt;/title&gt;\n"
"	&lt;titleabbrev id=\"final\"&gt;the final chapter&lt;/titleabbrev&gt;\n"
"\n"
"	&lt;para&gt;The secret to eternal life is…&lt;/para&gt;\n"
"&lt;/chapter&gt;\n"
"\n"
"[more deathless prose here]     \n"
"\n"
"The solution is in &lt;xref linkend=\"The_Secret\" endterm=\"final\"/&gt;."
msgstr ""
"\n"
"&lt;chapter id=\"The_Secret\"&gt;\n"
"\t&lt;title&gt;The Secret to Eternal Life&lt;/title&gt;\n"
"\t&lt;titleabbrev id=\"final\"&gt;the final chapter&lt;/titleabbrev&gt;\n"
"\n"
"\t&lt;para&gt;The secret to eternal life is&hellip;&lt;/para&gt;\n"
"&lt;/chapter&gt;\n"
"\n"
"[more deathless prose here]     \n"
"\n"
"The solution is in &lt;xref linkend=\"The_Secret\" endterm=\"final\"/&gt;.\n"
"\n"

msgid "The text surrounding the <tag>&lt;xref&gt;</tag> presents in the English version of the document as:"
msgstr "Come si vede, il testo che precede il tag <sgmltag>&lt;xref&gt;</sgmltag> presente nella versione inglese del documento è:"

msgid "The solution is in <literal>the final chapter</literal>."
msgstr "The solution is in <literal>the final chapter</literal>."

msgid "A translator sees the <tag>&lt;titleabbrev&gt;</tag> in a PO file as:"
msgstr "Un traduttore vede il tag <sgmltag>&lt;titleabbrev&gt;</sgmltag> nel file PO come:"

msgid ""
"#. Tag: titleabbrev\n"
"#, no-c-format\n"
"msgid \"the final chapter\"\n"
"msgstr \"\""
msgstr ""
"\n"
"#. Tag: titleabbrev\n"
"#, no-c-format\n"
"msgid \"the final chapter\"\n"
"msgstr \"\"\n"
"\n"

msgid "and sees the text that contains the <tag>&lt;xref&gt;</tag> elsewhere in the PO file (or, more likely, in a completely different PO file) as:"
msgstr "e vede il testo contenuto in <sgmltag>&lt;xref&gt;</sgmltag> in qualche altra parte del file PO (o, addirittura in un PO completamente diverso), come:"

msgid ""
"#. Tag: para\n"
"#, no-c-format\n"
"msgid \"The solution is in &lt;xref linkend=\\\"The_Secret\\\" endterm=\\\"final\\\"/&gt;.\"\n"
"msgstr \"\""
msgstr ""
"\n"
"#. Tag: para\n"
"#, no-c-format\n"
"msgid \"The solution is in &lt;xref linkend=\"The_Secret\" endterm=\"final\"/&gt;.\"\n"
"msgstr \"\"\n"
"\n"

msgid "The translator has no way of telling what will be substituted for <tag>&lt;xref linkend=\"The_Secret\" endterm=\"final\"/&gt;</tag> when the document builds, so a translation in Italian might read:"
msgstr "Il traduttore non ha idea di come verrà sostituito il tag <sgmltag>&lt;xref linkend=\"The_Secret\" endterm=\"final\"/&gt;</sgmltag> durante la creazione del documento, per cui una traduzione in italiano potrebbe leggersi come: "

msgid ""
"#. Tag: para\n"
"#, no-c-format\n"
"msgid \"The solution is in &lt;xref linkend=\\\"The_Secret\\\" endterm=\\\"final\\\"/&gt;.\"\n"
"msgstr \"La soluzione è in &lt;xref linkend=\\\"The_Secret\\\" endterm=\\\"final\\\"/&gt;.\""
msgstr ""
"\n"
"#. Tag: para\n"
"#, no-c-format\n"
"msgid \"The solution is in &lt;xref linkend=\"The_Secret\" endterm=\"final\"/&gt;.\"\n"
"msgstr \"La soluzione è in &lt;xref linkend=\"The_Secret\" endterm=\"final\"/&gt;.\"\n"
"\n"

msgid "Note the preposition <literal>in</literal>."
msgstr "Notare la preposizione <literal>in</literal>."

msgid "If the translator rendered <literal>the final chapter</literal> in Italian as <literal>l'ultimo capitolo</literal>, the result when the document builds will read:"
msgstr "Se il traduttore di lingua italiana traduce <literal>the final chapter</literal> in <literal>l'ultimo capitolo</literal>, il documento risultante si leggerebbe come:"

msgid "La soluzione è in <literal>l'ultimo capitolo</literal>."
msgstr "La soluzione è in <literal>l'ultimo capitolo</literal>."

msgid "This result is comprehensible, but inelegant, because Italian combines some of its prepositions with its definite articles. More elegant Italian would be:"
msgstr "Il risultato è comprensibile, ma poco elegante, per l'assenza di combinazione tra preposizione ed articolo. Una traduzione più elegante sarebbe stata:"

msgid "La soluzione è nell'<literal>ultimo capitolo</literal>."
msgstr "La soluzione è nell'<literal>ultimo capitolo</literal>."

msgid "Without knowing what text will appear in place of &lt;xref linkend=\"The_Secret\" endterm=\"final\"/&gt;, the translator into Italian cannot know whether to leave the preposition <literal>in</literal> to stand by itself, or which of seven different possible combinations with the definite article to use: <literal>nel</literal>, <literal>nei</literal>, <literal>nello</literal>, <literal>nell'</literal>, <literal>negli</literal>, <literal>nella</literal>, or <literal>nelle</literal>."
msgstr "Senza conoscere il testo che compare al posto di &lt;xref linkend=\"The_Secret\" endterm=\"final\"/&gt;, il traduttore in italiano non sa se usare la preposizione <literal>in</literal> invariata, o una delle sette possibili combinazioni con l'articolo determinativo: <literal>nel</literal>, <literal>nei</literal>, <literal>nello</literal>, <literal>nell'</literal>, <literal>negli</literal>, <literal>nella</literal> o <literal>nelle</literal>."

msgid "Furthermore, note that the combined preposition and article also poses a problem with regard to whether this word should be placed in the text surrounding the <tag>&lt;xref&gt;</tag>, or in the <tag>&lt;titleabbrev&gt;</tag>. Whichever of these two solutions the translator selects will cause problems when the <parameter>endterm</parameter> appears in other grammatical contexts, because not all Italian prepositions can combine with the definite article in this way."
msgstr "Inoltre, notare che la combinazione della preposizione con l'articolo pone anche una problema se inserire la preposizione articolata nel tag <sgmltag>&lt;xref&gt;</sgmltag> o nel tag <sgmltag>&lt;titleabbrev&gt;</sgmltag>. E comunque qualunque sia la soluzione scelta dal traduttore, ulteriori problemi si verificano quando l'<parameter>endterm</parameter> è presente in altri contesti grammaticali, poiché sarebbe richiesta un'altra preposizione articolata."

msgid "Due to the problems that <parameter>endterm</parameter> presents for translation, <application>Publican</application> discourages this attribute."
msgstr "A causa di queste difficoltà, in fase di traduzione di <parameter>endterm</parameter>, <application>Publican</application> non permette l'uso di questo attributo."

