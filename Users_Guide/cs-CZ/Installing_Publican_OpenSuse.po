# Josef Hruška <josef.hruska@upcmail.cz>, 2014. #zanata
msgid ""
msgstr ""
"Project-Id-Version: 0\n"
"POT-Creation-Date: 2014-10-03 13:17+1000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2014-07-31 01:53-0400\n"
"Last-Translator: Josef Hruška <josef.hruska@upcmail.cz>\n"
"Language-Team: Czech\n"
"Language: cs\n"
"X-Generator: Zanata 3.6.2\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2\n"

msgid "OpenSuse 12"
msgstr "OpenSuse 12"

msgid "<application>Publican</application> has not been usable on OpenSuse up until release 12.1. Certain dependencies were missing and could not be found in any known OpenSuse repository. This is not the case with OpenSuse 12.1 as all dependencies can now be found and installed."
msgstr "<application>Publican</application> nebylo možné používat v OpenSuse do vydání 12.1. Chyběly určité závislosti a v žádném známém repozitáři OpenSuse nebylo možné je nalézt. To už ovšem není případ OpenSuse 12.1, kde lze všechny závislosti vyhledat a nainstalovat. "

msgid "The following instructions describe installing <application>Publican</application> from source because, as yet, there is no <application>Publican</application> RPM for OpenSuse 12.1. The version of <application>Publican</application> is 2.9 taken directly from the source repository - previous versions have not been tested but may work."
msgstr "Následující pokyny popisují instalaci <application>Publicanu</application> ze zdrojových souborů, neboť dosud není pro OpenSuse 12.1 připraven RPM <application>Publicanu</application>. Verze 2.9 <application>Publicanu</application> je vzata přímo ze zdrojového repozitáře - dřívější verze nebyly testovány, mohou však fungovat."

msgid "At the time of writing, Publican 2.8 was the release version and work on 2.9 was still ongoing. For this reason the following instructions are subject to change."
msgstr "V době psaní tohoto dokumentu byla vydaná verze Publicanu 2.8 a na verzi 2.9 se stále pracovalo. Z tohoto důvodu mohou být následující pokyny předmětem změny."

msgid "The OpenSuse install was a default one with the following software categories added at install time:"
msgstr "Instalace OpenSuse byla výchozí, s těmito přidanými kategoriemi softwaru v době instalace:"

msgid "Technical Writing - for the Docbook tools etc."
msgstr "Tvorba technické dokumentace - pro nástroje Docbook, atd."

msgid "Perl Development"
msgstr "Vývoj Perl"

msgid "Web and LAMP Server"
msgstr "Web a LAMP Server"

msgid "The system used had KDE installed which shouldn't make a difference. The following KDE specific categories were also installed:"
msgstr "Použitý systém měl nainstalováno KDE, což by nemělo být nijak odlišné. Byly nainstalovány následující zvláštní kategorie KDE:"

msgid "KDE Development"
msgstr "Vývoj KDE"

msgid "Desktop Effects"
msgstr "Efekty pracovního prostředí"

msgid "Finally, the entire Games category was removed."
msgstr "A konečně byla zcela odstraněna kategorie Hry."

msgid "After OpenSuse had completed installing, and all current updates had been applied, the following steps were followed to install <application>Publican</application>."
msgstr "Po skončení instalace OpenSuse a provedení všech současných aktualizací byly učiněny pro účely instalace <application>Publicanu</application> tyto kroky:"

msgid "Open a terminal session."
msgstr "Otevřete sezení terminálu."

msgid "Install the dependencies that are available from various online repositories - many of these are not present in the installation DVD repository."
msgstr "Nainstalujte závislosti, které jsou k dispozici z různých online repozitářů - mnoho z nich se nenachází v repozitáři instalačního DVD."

msgid "<prompt>$</prompt> <command>sudo zypper install perl-Config-Simple perl-DateTime \\ perl-DateTime-Format-DateParse perl-DBD-SQLite perl-DBI \\ perl-File-Find-Rule perl-File-Which perl-HTML-Format \\ perl-Locale-MakeText-Gettext perl-Template-Toolkit \\ perl-Test-Deep perl-Test-Pod perl-XML-LibXSLT \\ perl-YAML liberation-fonts</command>"
msgstr ""
"<prompt>$</prompt> <command>sudo zypper install perl-Config-Simple perl-DateTime \\\n"
"perl-DateTime-Format-DateParse perl-DBD-SQLite perl-DBI \\\n"
"perl-File-Find-Rule perl-File-Which perl-HTML-Format \\\n"
"perl-Locale-MakeText-Gettext perl-Template-Toolkit \\\n"
"perl-Test-Deep perl-Test-Pod perl-XML-LibXSLT \\\n"
"perl-YAML liberation-fonts</command>"

msgid "<filename>Liberation-fonts</filename> is most likely already installed, but it is required. <application>Zypper</application> will not reinstall it if it is already present."
msgstr "Balíček <filename>Liberation-fonts</filename> je již nejspíše nainstalován, ale zkrátka se vyžaduje. <application>Zypper</application> ho nebude instalovat znovu, pokud se v systému již nachází."

msgid "Use <application>cpan</application> to install the remaining dependencies which cannot be installed by <application>zypper</application>:"
msgstr "Využijte <application>cpan</application> pro instalaci zbývajících závislostí, které nelze instalovat pomocí <application>zypper</application>:"

msgid "<prompt>$</prompt> <command>sudo sh cpan File::pushd File::Copy::Recursive Locale::PO pp \\ Syntax::Highlight::Engine::Kate XML::TreeBuilder exit </command>"
msgstr ""
"<prompt>$</prompt> <command>sudo sh\n"
"cpan File::pushd File::Copy::Recursive Locale::PO pp \\\n"
"Syntax::Highlight::Engine::Kate XML::TreeBuilder\n"
"exit\n"
"</command>"

msgid "Download the source code:"
msgstr "Stáhněte zdrojový kód:"

msgid "<prompt>$</prompt> <command>cd ~ mkdir -p SourceCode/publican cd SourceCode/publican svn checkout http://svn.fedorahosted.org/svn/publican/branches/publican-2x ./ </command>"
msgstr ""
"<prompt>$</prompt> <command>cd ~\n"
"mkdir -p SourceCode/publican\n"
"cd SourceCode/publican\n"
"svn checkout http://svn.fedorahosted.org/svn/publican/branches/publican-2x ./\n"
"</command>"

msgid "Build the <application>Publican</application> build script:"
msgstr "Sestavte sestavovací skript <application>Publicanu</application>:"

msgid "<prompt>$</prompt> <command>perl Build.PL </command>"
msgstr "<prompt>$</prompt> <command>perl Build.PL\n"
"</command>"

msgid "If all the dependencies are installed, you should see the following:"
msgstr "Jsou-li nainstalovány všechny závislosti, mělo by se zobrazit toto:"

msgid ""
"<computeroutput>WARNING: the following files are missing in your kit:</computeroutput>\n"
"<computeroutput> META.yml</computeroutput>\n"
"<computeroutput> Please inform the author.</computeroutput>\n"
"\n"
"<computeroutput>Created MYMETA.yml and MYMETA.json</computeroutput>\n"
"<computeroutput>Creating new 'Build' script for 'Publican' version '2.9'</computeroutput>"
msgstr ""
"<computeroutput>WARNING: the following files are missing in your kit:</computeroutput>\n"
"<computeroutput>        META.yml</computeroutput>\n"
"<computeroutput> Please inform the author.</computeroutput>\n"
"\n"
"<computeroutput>Created MYMETA.yml and MYMETA.json</computeroutput>\n"
"<computeroutput>Creating new 'Build' script for 'Publican' version '2.9'</computeroutput>"

msgid "If not, then use <application>cpan</application> (as root) to install the missing modules and run the build again. Replace any forward slashes '/' by a double colon '::' and make sure you use exactly the same letter case, for example: If <filename>File/pushd.pm</filename> is reported as missing, you would use this to install it:"
msgstr "Pokud ne, pak použitím <application>cpan</application> (jako uživatel root) nainstalujte chybějící moduly a spusťte opět sestavovací příkaz. Odstraňte všechna lomítka '/'pomocí dvojité dvojtečky '::' a ujistěte se, že jste použili přesně stejnou velikost písmen, na příklad: Je-li prohlášen soubor <filename>File/pushd.pm</filename> za chybějící, měli byste použít k jeho instalaci toto:"

msgid "<prompt>$</prompt> <command>sudo sh cpan File::pushd exit </command>"
msgstr ""
"<prompt>$</prompt> <command>sudo sh\n"
"cpan File::pushd\n"
"exit\n"
"</command>"

msgid "Assuming all went well, the <filename>Build.PL</filename> script will have created a new script named <filename>Build</filename> which we will use to create, test and install <application>Publican</application> 2.9."
msgstr "Za předpokladu, že vše proběhlo jak má, skript <filename>Build.PL</filename> vytvoří nový skript s názvem <filename>Build</filename>, který použijeme pro vytvoření, otestování a instalaci <application>Publicanu</application> 2.9."

msgid "<prompt>$</prompt> <command>./Build </command>"
msgstr "<prompt>$</prompt> <command>./Build\n"
"</command>"

msgid "There will be lots of text scrolling up the screen for a few minutes, you should eventually see the following:"
msgstr "Na obrazovku bude vypisováno po několik minut mnoho textu, nakonec byste měli spatřit toto:"

msgid "<computeroutput>DEBUG: Publican::Builder: end of build</computeroutput>"
msgstr "<computeroutput>DEBUG: Publican::Builder: end of build</computeroutput>"

msgid "Test the build:"
msgstr "Otestujte sestavení:"

msgid "<prompt>$</prompt> <command>./Build test </command>"
msgstr "<prompt>$</prompt> <command>./Build test\n"
"</command>"

msgid "Again, lots of scrolling text at the end of which you may see the following:"
msgstr "Opět bude probíhat dlouhý výpis textových informací, na jejichž konci byste měli narazit na toto:"

msgid ""
"<computeroutput>Test Summary Report</computeroutput>\n"
"<computeroutput>-------------------</computeroutput>\n"
"<computeroutput>t/910.publican.Users_Guide.t (Wstat: 256 Tests: 5 Failed: 1)</computeroutput>\n"
"<computeroutput> Failed test: 5</computeroutput>\n"
"<computeroutput> Non-zero exit status: 1</computeroutput>\n"
"<computeroutput>t/pod-coverage.t (Wstat: 256 Tests: 9 Failed: 1)</computeroutput>\n"
"<computeroutput> Failed test: 7</computeroutput>\n"
"<computeroutput> Non-zero exit status: 1</computeroutput>\n"
"<computeroutput>Files=10, Tests=68, 420 wallclock secs ( 0.31 usr 0.17 sys + 246.87 cusr 18.73 csys = 266.08 CPU)</computeroutput>\n"
"<computeroutput>Result: FAIL</computeroutput>\n"
"<computeroutput>Failed 2/10 test programs. 2/68 subtests failed.</computeroutput>"
msgstr ""
"<computeroutput>Test Summary Report</computeroutput>\n"
"<computeroutput>-------------------</computeroutput>\n"
"<computeroutput>t/910.publican.Users_Guide.t (Wstat: 256 Tests: 5 Failed: 1)</computeroutput>\n"
"<computeroutput>  Failed test:  5</computeroutput>\n"
"<computeroutput>  Non-zero exit status: 1</computeroutput>\n"
"<computeroutput>t/pod-coverage.t            (Wstat: 256 Tests: 9 Failed: 1)</computeroutput>\n"
"<computeroutput>  Failed test:  7</computeroutput>\n"
"<computeroutput>  Non-zero exit status: 1</computeroutput>\n"
"<computeroutput>Files=10, Tests=68, 420 wallclock secs ( 0.31 usr  0.17 sys + 246.87 cusr 18.73 csys = 266.08 CPU)</computeroutput>\n"
"<computeroutput>Result: FAIL</computeroutput>\n"
"<computeroutput>Failed 2/10 test programs. 2/68 subtests failed.</computeroutput>"

msgid "Don't worry. This is because of a missing <application>wkhtmltopdf</application> utility which is undergoing tests to be added to <application>Publican</application> in the future to replace Apache <application>FOP</application> as the pdf generation tool of choice. If <application>Publican</application> finds <application>wkhtmltopdf</application> it will use it, otherwise it uses <application>FOP</application>."
msgstr "Nelekejte se. Toto se děje kvůli chybějícímu pomocnému programu <application>wkhtmltopdf</application>, který prochází testy z důvodu přidaní do <application>Publicanu</application> v budoucnu, aby jako vybraný nástroj pro vytváření pdf nahradil Apache <application>FOP</application>. Nalezne-li <application>Publican</application> <application>wkhtmltopdf</application>, použije ho, jinak použije <application>FOP</application>."

msgid "Unfortunately, at the time of writing, because OpenSuse names one of the dependencies of <application>wkhtmltopdf</application> differently (<filename>ghostscript-fonts-std</filename> as opposed to <filename>ghostscript-fonts</filename>) <application>wkhtmltopdf</application> will not run even if force installed with no dependency checks."
msgstr "Naneštěstí v době psaní nazývá OpenSuse jednu ze závislostí <application>wkhtmltopdf</application> odlišně (<filename>ghostscript-fonts-std</filename> namísto <filename>ghostscript-fonts</filename>), proto <application>wkhtmltopdf</application> se nespustí, ani když bude instalován silou bez kontroly závislostí."

msgid "Install <application>wkhtmltopdf</application>."
msgstr "Nainstalujte <application>wkhtmltopdf</application>."

msgid "This step is optional. At the time of writing <application>wkhtmltopdf</application> did not work on OpenSuse 12.1 However, as the problems which prevent it working correctly from <application>Publican</application> may have been resolved, the following instructions give details on installing <application>wkhtmltopdf</application>."
msgstr "Tento krok je volitelný. V době psaní <application>wkhtmltopdf</application> na OpenSuse 12.1 nefungoval. Nicméně jelikož problémy, které brání jeho správné funkčnosti z <application>Publicanu</application>, mohou být vyřešeny, následující pokyny poskytují podrobnosti k instalaci <application>wkhtmltopdf</application>."

msgid "If you intend to create indices in your generated pdf documents, you are advised to use <application>Apache FOP</application> rather than <application>wkhtmltopdf</application>. With <application>FOP</application> you get actual page numbers which is better in a printed document."
msgstr "Zamýšlíte-li ve svých vytvořených dokumentech pdf vytvářet rejstříky, je vám doporučeno používat raději <application>Apache FOP</application> než <application>wkhtmltopdf</application>. S <application>FOP</application> získáte aktuální čísla stránek, což je pro tištěné dokumenty vhodnější."

msgid "<prompt>$</prompt> <command>JFEARN=http://jfearn.fedorapeople.org/wkhtmltopdf/f15 MYSYSTEM=i686 ## For 64bit system use MYSYSTEM=x86_64 instead. wget $JFEARN/$MYSYSTEM/wkhtmltopdf-qt-4.7.1-1.git20110804.fc15.i686.rpm wget $JFEARN/$MYSYSTEM/wkhtmltopdf-0.10.0_rc2-1.fc15.i686.rpm </command>"
msgstr ""
"<prompt>$</prompt> <command>JFEARN=http://jfearn.fedorapeople.org/wkhtmltopdf/f15\n"
"MYSYSTEM=i686\n"
"## For 64bit system use MYSYSTEM=x86_64 instead.\n"
"wget $JFEARN/$MYSYSTEM/wkhtmltopdf-qt-4.7.1-1.git20110804.fc15.i686.rpm\n"
"wget $JFEARN/$MYSYSTEM/wkhtmltopdf-0.10.0_rc2-1.fc15.i686.rpm\n"
"</command>"

msgid "If you use a 64 bit system, make sure to set <envar>MYSYSTEM</envar> appropriately."
msgstr "Provozujete-li 64-bitový systém, ujistěte se, že jste patřičně nastavili <envar>MYSYSTEM</envar>."

msgid "Once downloaded, install both rpms as follows:"
msgstr "Po jejich stažení nainstalujte oba rpm následovně:"

msgid "<prompt>$</prompt> <command>sudo sh rpm -ivh wkhtmltopdf-qt* rpm -ivh --nodeps wkhtmltopdf-0* exit </command>"
msgstr ""
"<prompt>$</prompt> <command>sudo sh\n"
"rpm -ivh wkhtmltopdf-qt*\n"
"rpm -ivh --nodeps wkhtmltopdf-0*\n"
"exit\n"
"</command>"

msgid "You have to use the option to ignore dependencies on the latter rpm due to the <filename>ghostscript-fonts</filename> problem described above."
msgstr "Musíte použít volbu, kdy ignorujete závislosti druhého rpm kvůli výše popsaným problémům <filename>ghostscript-fonts</filename>."

msgid "Install <application>Publican</application>."
msgstr "Nainstalujte <application>Publican</application>."

msgid "The final stage is to install Publican, even though the testing stage had a couple of sub-tests which failed."
msgstr "Závěrečnou fází je instalace Publicanu, i když testovací fáze měla pár podtestů, které selhaly."

msgid "<prompt>$</prompt> <command>sudo sh ./Build test exit </command>"
msgstr ""
"<prompt>$</prompt> <command>sudo sh\n"
"./Build test\n"
"exit\n"
"</command>"

msgid "The following steps are optional but it's a good idea to test that everything is working before you spend time on your own documents."
msgstr "Následující kroky jsou volitelné, ale vždy je dobrý nápad otestovat, zda vše funguje předtím, než strávíte čas na svých dokumentech."

msgid "Test the installed <application>Publican</application> build:"
msgstr "Otestujte nainstalované sestavení <application>Publicanu</application>:"

msgid ""
"<prompt>$</prompt> <command>publican create --type=book --product=testing --version=1.2.3 --name=TestPublican</command>\n"
"  Processing file en-US/Author_Group.xml -&gt; en-US/Author_Group.xml\n"
"  Processing file en-US/Book_Info.xml -&gt; en-US/Book_Info.xml\n"
"  Processing file en-US/Chapter.xml -&gt; en-US/Chapter.xml\n"
"  Processing file en-US/Preface.xml -&gt; en-US/Preface.xml\n"
"  Processing file en-US/Revision_History.xml -&gt; en-US/Revision_History.xml\n"
"  Processing file en-US/TestPublican.xml -&gt; en-US/TestPublican.xml\n"
"\n"
"<prompt>$</prompt> <command>cd TestPublican/ publican build --lang=all --formats=html,html-single,html-desktop,txt,pdf,epub</command>"
msgstr ""
"<prompt>$</prompt> <command>publican create --type=book --product=testing --version=1.2.3 --name=TestPublican</command>\n"
"  Processing file en-US/Author_Group.xml -&gt; en-US/Author_Group.xml\n"
"  Processing file en-US/Book_Info.xml -&gt; en-US/Book_Info.xml\n"
"  Processing file en-US/Chapter.xml -&gt; en-US/Chapter.xml\n"
"  Processing file en-US/Preface.xml -&gt; en-US/Preface.xml\n"
"  Processing file en-US/Revision_History.xml -&gt; en-US/Revision_History.xml\n"
"  Processing file en-US/TestPublican.xml -&gt; en-US/TestPublican.xml\n"
"\n"
"<prompt>$</prompt> <command>cd TestPublican/\n"
"publican build --lang=all --formats=html,html-single,html-desktop,txt,pdf,epub</command>\n"

msgid "At the time of writing, creating epubs with <application>Publican</application> 2.9 on OpenSuse gave the following error:"
msgstr "V době psaní vytváření souborů epub <application>Publicanem</application> 2.9 v OpenSuse vytvářelo následující chybu:"

msgid ""
"<computeroutput>runtime error: file /usr/share/publican/xsl/epub.xsl element choose</computeroutput>\n"
"<computeroutput>Variable 'epub.embedded.fonts' has not been declared.</computeroutput>\n"
"<computeroutput> at /usr/lib/perl5/site_perl/5.14.2/Publican/Builder.pm line 915</computeroutput>"
msgstr ""
"<computeroutput>runtime error: file /usr/share/publican/xsl/epub.xsl element choose</computeroutput>\n"
"<computeroutput>Variable 'epub.embedded.fonts' has not been declared.</computeroutput>\n"
"<computeroutput> at /usr/lib/perl5/site_perl/5.14.2/Publican/Builder.pm line 915</computeroutput>"

msgid "No epub file was created. The individual working files were however, and can be built into an epub book using <application>Sigil</application>, if desired."
msgstr "Nebyl vytvořen žádný epub. Jednotlivé pracovní soubory však ano, a chcete-li, lze je sestavit do knihy epub použitím <application>Sigil</application>."

msgid "Using the <application>Dolphin</application> file manager, you can browse to <filename class=\"directory\">SourceCode/TestPublican/tmp/en-US/</filename> and view the various output formats that you find there."
msgstr "Správcem souborů <application>Dolphin</application> můžete přejít do adresáře <filename class=\"directory\">SourceCode/TestPublican/tmp/en-US/</filename> a prohlédnout si různé výstupní formáty, které zde naleznete."

